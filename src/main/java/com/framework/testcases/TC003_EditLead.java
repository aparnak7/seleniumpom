package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class TC003_EditLead extends ProjectMethods {
	@BeforeTest
	public void setData() {
		testCaseName="TC003_EditLead";
		testDescription="creating a lead";
		testNodes="Leads";
		author="Gayathri";
		category="smoke";
		dataSheetName="TC003";
		
	}
   @Test(dataProvider="fetchData")
	public void EditLead(String username,String password,String firstname,String companyname) {
		new LoginPage()
		.enterUsername(username)
		.enterPassword(password)
		.clickLogin()
		.clickCrmsfa()
		.clickLeads()
		.clickFindLeadslink()
		.enterFirstName(firstname)
		.clickFindLeadsBtn()
		.clickFirstRecord()
		.verifyfirstname(firstname)
		.clickEditLeadBtn()
		.updateCompanyName(companyname)
		.clickUpdate()
		.verifyCompanyName(companyname);
		
	
	
	}
	
}
