package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class TC002_CreateLead extends ProjectMethods {
	@BeforeTest
	public void setData() {
		testCaseName="TC002_CreateLead";
		testDescription="creating a lead";
		testNodes="Leads";
		author="Gayathri";
		category="smoke";
		dataSheetName="TC002";
		
	}
   @Test(dataProvider="fetchData")
	public void CreateLead(String username,String password,String companyname,String firstname,String lastname) {
		new LoginPage()
		.enterUsername(username)
		.enterPassword(password)
		.clickLogin()
		.clickCrmsfa()
		.clickLeads()
		.clickCreateLeadlink()
		.enterCompanyName(companyname)
		.enterFirstName(firstname)
		.enterLastName(lastname)
		.clickCreateLead()
		.verifyfirstname(firstname);
		
		
	
	}
	
}
