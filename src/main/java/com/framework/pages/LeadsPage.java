package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class LeadsPage extends ProjectMethods {
	
	public LeadsPage() {
		//apply PageFactory
				PageFactory.initElements(driver, this); 
			}
	
	@FindBy(how = How.LINK_TEXT,using="Create Lead") WebElement eleCreatelead;
	@FindBy(how=How.LINK_TEXT,using="Find Leads") WebElement eleFindlead;

	public CreateLeadPage clickCreateLeadlink() {
		//WebElement eleLogin = locateElement("class", "decorativeSubmit");
	    click(eleCreatelead);
	    /*HomePage hp = new HomePage();
	    return hp;*/ 
	    return new CreateLeadPage();
	}
	
	public FindLeadsPage clickFindLeadslink() {
		 click(eleFindlead);
		 return new FindLeadsPage();
	}
	
}
