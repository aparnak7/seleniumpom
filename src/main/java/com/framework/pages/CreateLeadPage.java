package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class CreateLeadPage extends ProjectMethods {

	public CreateLeadPage() {
		//apply PageFactory
				PageFactory.initElements(driver, this); 
			}
	@FindBy(how = How.ID,using="createLeadForm_companyName") WebElement eleCname;
	@FindBy(how = How.ID,using="createLeadForm_firstName") WebElement eleFname;
	@FindBy(how = How.ID,using="createLeadForm_lastName") WebElement eleLname;
	@FindBy(how = How.NAME,using="submitButton") WebElement eleClickcreatelead;
	
	public CreateLeadPage enterCompanyName(String data) {
		clearAndType(eleCname, data);
		return this;
	}
	public CreateLeadPage enterFirstName(String data) {
		clearAndType(eleFname, data);
		return this;
	}
	public CreateLeadPage enterLastName(String data) {
		clearAndType(eleLname, data);
		return this;
	}
	public ViewLeadsPage clickCreateLead() {
		click(eleClickcreatelead);
		return new ViewLeadsPage();
	}
}
