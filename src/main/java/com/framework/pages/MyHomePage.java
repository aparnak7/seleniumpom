package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class MyHomePage extends ProjectMethods {
	
	public MyHomePage() {
		//apply PageFactory
		PageFactory.initElements(driver, this); 
	}
	
	@FindBy(how = How.XPATH,using="//a[text()='Leads']") WebElement eleLeadslink;
	
	public LeadsPage clickLeads() {
		//WebElement eleLogin = locateElement("class", "decorativeSubmit");
	    click(eleLeadslink);
	    /*HomePage hp = new HomePage();
	    return hp;*/ 
	    return new LeadsPage();
}
}