package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class EditLeadPage extends ProjectMethods {

	public EditLeadPage() {
		//apply PageFactory
		PageFactory.initElements(driver, this); 
	
	}
	@FindBy(how = How.XPATH,using="(//input[@name=\"companyName\"])[2]") WebElement eleCname;
	@FindBy(how=How.NAME,using="submitButton") WebElement eleClkUpdate;
	
	public EditLeadPage updateCompanyName(String data) {
		clearAndType(eleCname, data);
		return this;
	}
	public ViewLeadsPage clickUpdate() {
		click(eleClkUpdate);
		return new ViewLeadsPage();
	}
}
