package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class FindLeadsPage extends ProjectMethods {
	
	public FindLeadsPage() {
		//apply PageFactory
				PageFactory.initElements(driver, this); 
			}
	@FindBy(how = How.XPATH,using="(//input[@name='firstName'])[3]") WebElement eleFname;
	@FindBy(how = How.XPATH,using="//button[text()='Find Leads']") WebElement eleFindLeadsBtn;
	@FindBy(how = How.XPATH,using="(//a[@class='linktext'])[4]") WebElement eleFirstRecord;
	
	public FindLeadsPage enterFirstName(String data) {
		clearAndType(eleFname, data);
		return this;
	}
	public FindLeadsPage clickFindLeadsBtn() {
		click(eleFindLeadsBtn);
		return this;
	}
		public ViewLeadsPage clickFirstRecord() {
			click(eleFirstRecord);
			return new ViewLeadsPage();
			
		}
	}
