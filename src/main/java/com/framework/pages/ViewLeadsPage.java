package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class ViewLeadsPage extends ProjectMethods{
	
	public ViewLeadsPage() {
		//apply PageFactory
		PageFactory.initElements(driver, this); 
	
	}
	@FindBy(how = How.ID,using="viewLead_firstName_sp") WebElement eleVerifylead;
	@FindBy(how = How.XPATH,using="//div[@id=\"sectionHeaderTitle_leads\"]") WebElement eleViewLeadTitle;
	@FindBy(how=How.XPATH,using="//a[text()='Edit']") WebElement eleEditLeadBtn;
	@FindBy(how=How.ID,using="viewLead_companyName_sp") WebElement eleCnameVerify;
	
	
	public ViewLeadsPage verifyfirstname(String data) {
		verifyExactText(eleVerifylead,data);
		return this;
		
	}
	public ViewLeadsPage verifyViewLeadTitle(String data) {
		verifyExactText(eleViewLeadTitle,data);
		return this;
	}
	public EditLeadPage clickEditLeadBtn() {
		click(eleEditLeadBtn);
		return new EditLeadPage();
	}
	
	public ViewLeadsPage verifyCompanyName(String data) {
		verifyPartialText(eleCnameVerify,data);
		return this;
	}
	
	
}
