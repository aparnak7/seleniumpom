package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class HomePage extends ProjectMethods{

	public HomePage() {
       PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.CLASS_NAME,using="decorativeSubmit") WebElement eleLogout;
	@FindBy(how = How.PARTIAL_LINK_TEXT,using="CRM/SFA") WebElement eleCrmsfa;
	
	public LoginPage clickLogout() {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleLogout);
		return new LoginPage(); 
	}
	public MyHomePage clickCrmsfa() {
		//WebElement eleLogin = locateElement("class", "decorativeSubmit");
	    click(eleCrmsfa);
	    /*HomePage hp = new HomePage();
	    return hp;*/ 
	    return new MyHomePage();

}
}
















